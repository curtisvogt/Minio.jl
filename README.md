# Minio

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Minio.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/Minio.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/Minio.jl/-/pipelines)

Unofficial Julia tools for interacting with [min.io](https://min.io/) data storage service through
the Julia [AWS S3](https://github.com/JuliaCloud/AWSS3.jl) interface wrapper.
